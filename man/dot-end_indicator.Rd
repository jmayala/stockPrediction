% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/strategies.R
\name{.end_indicator}
\alias{.end_indicator}
\title{End of series indicator.}
\usage{
.end_indicator(x)
}
\arguments{
\item{x}{data}
}
\value{
returns 1 on the second to last observation
}
\description{
End of series indicator.
}
\examples{
.end_indicator(AAPL)
}
