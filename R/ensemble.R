#' GBM Ensemble learning model
#'
#' Crea un modelo ensemble con GBM. El ensemble esta compuesto por los siguientes modelos:
#' \itemize{
#'   \item Evolutionary Decision Trees
#'   \item Random Forest
#'   \item Neural Networks
#' }
#'
#' Los parámetros \code{training} y \code{testing} deben estar formateados correctamente para la prediccion.
#' Solo incluir las columnas requeridas para la ventana historica. (Para w=24, incluir columnas 1:24).
#'
#' @param training data.frame para entrenamiento (Ver detalles)
#' @param testing data.frame para validacion (Ver detalles)
#' @param h horizonte de prediccion
#' @param w ventana historica
#' @param dataset nombre del dataset
#' @param update funcion a invocar en cada paso intermedio. Puede usarse para log.
#'
#' @export
#'
#' @return lista con resultados y modelos obtenidos
#'
ensemble <- function(training, testing, h, w,
                     dataset = "NA",
                     update = NULL) {
  if (is.null(update) || !is.function(update)) {
    update <- function(...) {}
  }

  update(key_value_str(list(`Historic Window` = w, `Prection horizon` = h, `Dataset` = dataset), collapse = ", ", sep = ": "))

  set.seed(123)

  # Columna a predecir
  col_predict <- w + 1

  # Make sure we only use the historical window
  training_data <- training[, 1:w]
  testing_data <- testing[, 1:w]

  # saving the predictors to be used (needed for NN)
  predictors <- names(training_data)

  training_data$y <- training[[col_predict]]
  testing_data$y <- testing[[col_predict]]

  train_predictions <- data.frame(
    Date = training$index,
    Real = training_data$y
  )

  test_predictions <- data.frame(
    Date = testing$index,
    Real = testing_data$y
  )

  #### RANDOM FOREST model #####
  update("Entrenando Random Forest...")

  tryCatch(model_rf <- randomForest::randomForest(y ~ ., data = training_data, ntree = 100, maxnodes = 100, prox = TRUE),
    error = function(e) message(e)
  )

  update("Haciendo predicciones con Random Forest...")
  test_predictions$RF <- predict(model_rf, testing_data)
  train_predictions$RF <- predict(model_rf, training_data)

  update("Entrenamiento RF terminado")

  #### EVtree model ####
  update("Entrenando evtree...")
  model_ev <- evtree::evtree(y ~ .,
    data = training_data,
    control = evtree::evtree.control(
      minbucket = 8,
      minsplit = 100,
      maxdepth = 15,
      ntrees = 300,
      niterations = 1000,
      alpha = 0.25,
      operatorprob = list(
        pmutatemajor = 0.2,
        pmutateminor = 0.2,
        pcrossover = 0.8,
        psplit = 0.2,
        pprune = 0.4
      )
    )
  )

  update("Haciendo predicciones con evtree...")
  test_predictions$EV <- predict(model_ev, testing_data)
  train_predictions$EV <- predict(model_ev, training_data)

  update("Entrenamiento evtree terminado")


  #### NN model ####
  update("Entrenando Neural Network...")

  f <- as.formula(paste("y ~", paste(predictors, collapse = " + ")))
  model_nn <- nnet::nnet(f, training_data, size = 10, linout = TRUE, skip = TRUE, MaxNWts = 10000, trace = FALSE, maxit = 1000)
  # Las predicciones de nn retornan una matriz
  update("Haciendo predicciones con Neural Network...")
  test_predictions$NN <- as.numeric(predict(model_nn, testing_data))
  train_predictions$NN <- as.numeric(predict(model_nn, training_data))

  update("Entrenamietno Neural Network terminado")

  #### Consolidate testing and training data #####
  # add prediction to test data
  testing_data$Pred_EV <- test_predictions$EV
  testing_data$Pred_RF <- test_predictions$RF
  testing_data$Pred_NN <- test_predictions$NN

  # add prediction to training and test sets for later steps
  training_data$Pred_EV <- train_predictions$EV
  training_data$Pred_RF <- train_predictions$RF
  training_data$Pred_NN <- train_predictions$NN


  #### TOP LAYER OF ENSEMBLE ####
  update("Construyendo ensemble...")
  # use lm to build emsamble, check it out, it's too simple now
  # model_lm = lm(X169 ~ Pred_EV + Pred_RF + Pred_NN, data=data_p_training, na.action = NULL)
  model_gbm <- gbm::gbm(y ~ Pred_EV + Pred_RF + Pred_NN,
    data = training_data,
    distribution = "gaussian", n.trees = 5,
    interaction.depth = 8, shrinkage = 1, n.minobsinnode = 1
  )

  # predict
  update("Realizando predicciones con ensemble...")
  test_predictions$GBM <- predict(model_gbm, testing_data, n.trees = 5)
  train_predictions$GBM <- predict(model_gbm, training_data, n.trees = 5)

  update("Ensemble terminado")

  #### Results ####
  results <- list()
  params <- list(src = dataset, w = w, h = h)

  results$params <- params
  results$models$ev <- model_ev
  results$models$rf <- model_rf
  results$models$nn <- model_nn
  results$models$gbm <- model_gbm

  results$test_predictions <- test_predictions
  results$train_predictions <- train_predictions

  results$mre$test <- test_predictions %>%
    tidyr::gather(Algoritmo, Prediccion, -c(Date, Real)) %>%
    dplyr::group_by(Algoritmo) %>%
    dplyr::summarise(MRE = mean(abs(Real - Prediccion) / Real) * 100)

  results$mre$train <- train_predictions %>%
    tidyr::gather(Algoritmo, Prediccion, -c(Date, Real)) %>%
    dplyr::group_by(Algoritmo) %>%
    dplyr::summarise(MRE = mean(abs(Real - Prediccion) / Real) * 100)

  results$name <- paste0("ensemble_", key_value_str(params, collapse = "_", sep = ""))

  results
}

#' Plot predictions from ensemble and its models
#'
#' @param df predictions data.frame
#' @param params list parameters used for the ensemble
#'
#' @return a ggplot object
#' @import ggplot2
#' @export
#'
#' @examples
ensemble_plot_predictions <- function(df, params) {
  params_title <- paste0("(", key_value_str(params, collapse = ", ", sep = ": "), ")")

  df %>%
    tidyr::gather(Algoritmo, Precio, -Date) %>%
    ggplot(aes(x = Date, y = Precio)) +
    geom_line(aes(color = Algoritmo)) +
    scale_x_datetime(date_labels = "%b %d") +
    xlab("Fecha") + ggtitle(paste("Testing Data", params_title))
}


#' Ejecuta el ensemble learner
#'
#' Entrena el ensemble con los datos proveidos. Puede ser un xts
#'
#' @param x xts con los datos OHLC
#' @param h horizonte de prediccion
#' @param w tamaño de la ventana
#' @param dataset nombre del dataset
#' @param update function para mostrar progreso
#' @param use_options use opciones de max_window y max_horizon
#'
#' @return lista con los resultados
#' @export
#'
#' @examples
#' ensemble_learn(AAPL, 1, 24, "AAPL")
ensemble_learn <- function(data, h, w, dataset, update = NULL, use_options = FALSE) {
  w_max <- w
  h_max <- h

  if (is.null(update) || !is.function(update)) {
    update <- function(...) {}
  }

  if (!inherits(data, "ts.prediction")) {
    data <- time_series_prediction_format(data)
    use_options <- TRUE
  }

  if (use_options) {
    w_max <- getOption("max_historic_window", default = 168)
    h_max <- getOption("max_prediction_horizon", default = 24)
  }

  dates <- data$index
  matrix <- data[, c(1:w, w_max + h)] # Historic Window | Prediction
  matrix$index <- dates
  matrix <- tidyr::drop_na(matrix)

  # Split the matrix to data_training and data_test
  split <- 0.7
  corte <- floor(split * nrow(matrix))
  training <- matrix[1:corte, ]
  testing <- matrix[(corte + 1):nrow(matrix), ]
  update(paste("dataset: ", dataset, ". nrow: ", nrow(matrix), ". Split: ", split, ". Corte: ", corte, "."))

  ensemble(training, testing, h = h, w = w, dataset = dataset, update = update)
}
