#' Tabla de proporciones.
#'
#' @param dataframe el dataframe con los clusters.
#'
#' @return tabla de proporciones.
#' @export
proptable <- function(dataframe) {
  # Cluster del siguiente dia
  crosstab <- table(dataframe$cluster, lag(dataframe$cluster, 1))
  prop_table <- round(prop.table(crosstab, 1) * 100, 2)
  return(prop_table)
}

#' Grafica clusters.
#'
#' @param dataframe el dataframe con los clusters.
#' @param title el titulo del grafico.
#' @param colors vector con los colores.
#'
#' @return grafico con los clusters.
#' @export
clusplot <- function(dataframe, title, colors) {
  begin <- format(start(dataframe), "%Y")
  finish <- format(end(dataframe), "%Y")
  if (missing(title)) {
    title <- deparse(substitute(dataframe))
  }
  if (missing(colors)) {
    colors <- c("#ec1754", "#76db56", "#a31449", "#10d7ac", "#fe6c8e", "#d9c309", "#bea0fc", "#f97c20", "#b4a0d6", "#2a6026", "#90567b", "#e9bd6c")
  }
  k <- length(levels(factor(dataframe$cluster)))

  clusters <- ggplot(dataframe, aes(real_body_normalized, shadow_symmetry, col = factor(cluster))) +
    geom_point() +
    scale_color_manual(values = colors) +
    scale_fill_manual(values = colors) +
    xlab("Real Body Normalized") +
    ylab("Shadow Symmetry") +
    labs(col = "Cluster") +
    ggtitle(sprintf("%s (k=%d) %s / %s", title, k, begin, finish))

  clusters
}

#' Realiza analisis de clusters.
#'
#' @param dataframe el dataframe.
#' @param title el titulo del analisis.
#' @param pdf guardar en pdf.
#'
#' @return lista con los resultados.
#' @export
analyze <- function(dataframe, title, pdf = FALSE) {
  k <- length(levels(factor(dataframe$cluster)))
  begin <- start(dataframe) # inicio del xts
  finish <- end(dataframe) # fin del xts

  # Cluster del siguiente dia
  crosstab <- table(dataframe$cluster, lag(dataframe$cluster, 1))
  prop_table <- round(prop.table(crosstab, 1) * 100, 2)

  print(sprintf("Analyzing %s %s / %s", title, begin, finish))
  print(prop_table)

  # Mayores probabilidades
  greatest <- head(order(prop_table, decreasing = TRUE), 2)
  clust <- arrayInd(greatest, dim(prop_table))

  colors_high <- c("#ec1754", "#76db56", "#a31449", "#10d7ac", "#fe6c8e", "#d9c309", "#bea0fc", "#f97c20", "#b4a0d6", "#2a6026", "#90567b", "#e9bd6c")
  colors_down <- c("#d72f40", "#5bb444", "#ab3b58", "#4cbb99", "#e9738c", "#baaa27", "#9163cc", "#ce612e", "#6788ce", "#628341", "#bd5fab", "#b28a49")

  up.default <- "black"
  dn.default <- "white"
  normal_theme <- chart_theme()
  normal_theme$col$up.col <- up.default
  normal_theme$col$dn.col <- dn.default

  htheme1 <- chart_theme()
  htheme1$col$up.col <- if_else(dataframe$cluster %in% clust[1, ], colors_high[dataframe$cluster], up.default)
  htheme1$col$dn.col <- if_else(dataframe$cluster %in% clust[2, ], colors_down[dataframe$cluster], dn.default)

  htheme2 <- chart_theme()
  htheme2$col$up.col <- if_else(dataframe$cluster %in% clust[2, ], colors_high[dataframe$cluster], up.default)
  htheme2$col$dn.col <- if_else(dataframe$cluster %in% clust[2, ], colors_down[dataframe$cluster], dn.default)

  if (pdf) {
    filename <- paste(title, "k", k, format(begin, "%Y"), format(finish, "%Y"), sep = "-")
    filename <- file.path(".", "figs", paste0(filename, ".pdf"))
    pdf(filename)
    cat("\nSaving to", filename, "\n")
  }

  par(mfrow = c(1, 1))
  clusters <- clusplot(dataframe, "DJI")

  par(mfrow = c(2, 1))

  normal <- chart_Series(dataframe, name = paste0(title, " Normal"), theme = normal_theme, subset = "2016-10::")
  bull_line <- xts(dataframe$Close > dataframe$Open, index(dataframe)) # resalta las velas alcistas

  print(sprintf("Grafico 1 P( %d | %d ) = %.2f", clust[1, 1], clust[1, 2], prop_table[greatest[1]]))
  highlight1 <- chart_Series(dataframe,
    name = sprintf("%s Cluster %d y %d", title, clust[1, 2], clust[1, 1]),
    theme = htheme1, subset = "2016-10::"
  )
  add_TA(bull_line, on = -1, col = "lightblue")

  print(sprintf("Grafico 2 P( %d | %d ) = %.2f", clust[2, 1], clust[2, 2], prop_table[greatest[2]]))
  highlight2 <- chart_Series(dataframe,
    name = sprintf("%s Cluster %d y %d", title, clust[2, 2], clust[2, 1]),
    theme = htheme2, subset = "2016-10::"
  )
  add_TA(bull_line, on = -1, col = "lightblue")
  list(
    table = prop_table,
    graphics = list(clusters, normal, highlight1, normal, highlight2)
  )
}
