#' Exporta el dataset a un archivo csv
#'
#' @param data el dataset
#' @param file path del archivo a exportar
#'
#' @return el archivo csv
#' @export
#'
#' @examples
#' export_csv(DJI)
export_csv <- function(data, file) {
  df <- to_dataframe(data)
  write.csv(df, file, row.names = FALSE, quote = FALSE)
}

#' OHLC Input
#'
#' @description
#' Lee datos OHLC de un archivo CSV.
#'
#' @param file el nombre del archivo
#' @param header indicador lógico que indica si el archivo contiene las variables en la primera línea.
#' @param date nombre para la columna de fecha.
#' @param open nombre para el valor `Open`.
#' @param high nombre para el valor `High`.
#' @param low nombre para el valor `Low`.
#' @param close nombre para el valor `Close`.
#' @param extra_cols si es `FALSE`, solo se agregan columnas OHLC y de Fecha. Si es `TRUE` se agregan todas las columnas.
#' @param time nombre opcional de la columna con la hora de trading (Sólo para velas intraday).
#' @param adjusted nombre opcional de la columna con el Cierre Ajustado.
#' @return un `xts` con los valores OHLC.
#'
#' @importFrom utils read.csv
#'
#' @export
read_ohlc <- function(file, header = TRUE, date = "Date",
                      open = "Open", high = "High", low = "Low", close = "Close",
                      extra_cols = FALSE,
                      time, adjusted) {
  data <- read.csv(file, stringsAsFactors = FALSE, header = header)

  cols <- c(open, high, low, close)
  if (extra_cols) {
    extra <- setdiff(colnames(data), cols)
    cols <- c(cols, extra)
  }

  # Extra fecha
  dates_column <- data[, date]
  if (!missing(time)) {
    # Si la hora esta en otra columna se concatena con la fecha
    dates_column <- paste(data[, date], data[, time])
  }

  aux.date <- lubridate::parse_date_time(dates_column, "Ymd HMS", truncated = 3)

  # Reemplaza close con el adjusted close
  if (!missing(adjusted)) {
    data[, close] <- data[, adjusted]
  }

  # Filtra columnas elegidas
  data <- data[, cols]
  # Solo mantiene columnas numericas
  data <- data[, sapply(data, is.numeric)]
  # Normaliza nombre de columnas
  data_xts <- xts(data, order.by = aux.date)
  col_titles <- unlist(lapply(colnames(data_xts), title_case))
  colnames(data_xts) <- col_titles
  data_xts
}

#' Guarda modelos a un archivo .rda
#'
#' @param model modelo a guardar
#' @param model_name nombre del modelo
#' @param params lista con parametros y valores utilizados al entrenar el modelo
#' @param dir directorio donde se guarda el modelo
#' @param createDir si es \code{TRUE}, se crea el directorio si es que no existe.
#' @param ... argumentos adicionales para \code{\link[base]{save}}
#'
#' @export
#'
#' @examples
#' ctl <- c(4.17, 5.58, 5.18, 6.11, 4.50, 4.61, 5.17, 4.53, 5.33, 5.14)
#' trt <- c(4.81, 4.17, 4.41, 3.59, 5.87, 3.83, 6.03, 4.89, 4.32, 4.69)
#' group <- gl(2, 10, 20, labels = c("Ctl", "Trt"))
#' weight <- c(ctl, trt)
#' lm.D9 <- lm(weight ~ group)
#' save_model(lm.D9)
save_model <- function(model = stop("'model' debe ser especificado"),
                       model_name = deparse(substitute(model)),
                       params = NULL, dir = ".", createDir = TRUE, ...) {
  if (!dir.exists(dir)) {
    dir.create(dir)
  }

  params_string <- ifelse(is.null(params), "", key_value_str(params, collapse = "_", sep = ""))
  file <- paste0(paste(model_name, params_string, sep = "_"), ".rda")

  file <- file.path(dir, file)

  save(model, file = file, ...)
}

#' Read OHLCV csv
#'
#' First column must be the date, and the second must be the time component (optional)
#'
#' @param filename path to file
#'
#' @return a xts
#' @export
#'
#' @examples
#' read_ohlc("ohlc.csv")
read_ohlcv <- function(filename) {
  # Check if headers are present
  first <- data.table::fread(filename, header = FALSE, nrows = 1)
  if (is.character(first$V3)) {
    skip <- 1
  } else {
    skip <- 0
  }
  dt <- data.table::fread(filename, header = FALSE, skip = skip)

  if (is.character(dt$V2)) {
    # Has time column
    dt[, V1 := as.POSIXct(paste(V1, V2), format = "%Y-%m-%d %H:%M", tz = "UTC")]
    dt[, V2 := NULL]
  } else {
    dt[, V1 := as.POSIXct(V1, format = "%Y-%m-%d", tz = "UTC")]
  }

  x <- data.table::as.xts.data.table(dt)
  colnames(x) <- c("Open", "High", "Low", "Close", "Volume")
  x
}

#' Obtiene un dataset del formato csv
#'
#' @param dataset nombre del dataset
#' @param datadir directorio en donde se encuentra el dataset
#' @param recursive buscar recursivamente el dataset dentro del `datadir``
#'
#' @return el xts con los datos
#' @export
#'
#' @examples
#' ## Not Run
#' read_dataset("US30D1", datadir = "~/Downloads/datos_ohlc")
read_dataset <- function(dataset, datadir, recursive = TRUE, verbose = FALSE) {
  extension <- ".csv"
  filename <- paste0(dataset, extension)
  files <- list.files(datadir, pattern = filename, recursive = recursive, full.names = TRUE)
  file <- files[1]
  if (is.na(file)) {
    stop(filename, " dataset was not found in ", datadir)
  }
  if (verbose) {
    if (length(files) > 1) {
      message("Multiple datasets for ", dataset, "found")
    }
    message("Loading ", file)
  }
  read_ohlcv(file)
}

#' Carga el dataset en el entorno global
#'
#' @param dataset nombre del dataset
#' @param datadir directorio en donde se encuentra el dataset
#' @param envir entorno donde asignar la variable
#' @param recursive buscar recursivamente el dataset dentro del `datadir``
#'
#' @return el nombre de la variable a la que se asigno el dataset
#' @export
#'
#' @examples
#' ## Not Run
#' load_dataset("US30D1", datadir = "~/Downloads/datos_ohlc")
load_dataset <- function(dataset, datadir, envir = .GlobalEnv, recursive = TRUE) {
  data <- read_dataset(dataset, datadir, recursive = recursive)
  assign(dataset, data, envir = envir)

  dataset
}
