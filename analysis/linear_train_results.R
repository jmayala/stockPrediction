# Combina resultados del entrenamiento lineal

library(tidyverse)

metrics <- function(results_file) {
  message("Loading ", results_file, "...")
  results <- readRDS(results_file)

  tryCatch({
    train <- results$train_predictions %>% mutate(Set = "train")
    test <- results$test_predictions %>% mutate(Set = "test")
    predictions <- bind_rows(train, test) %>% as_tibble()

    measure <- predictions %>%
      gather(Algoritmo, Prediccion, -c(Date, Real, Set)) %>%
      group_by(Algoritmo, Set) %>%
      summarize(
        MRE = (abs(Real - Prediccion) / Real) %>% mean(na.rm = T),
        MAE = (abs(Real - Prediccion)) %>% mean(na.rm = T),
        sMAPE = (2 * abs(Real - Prediccion) / abs(Real + Prediccion)) %>% mean(na.rm = T),
        SSE = sum((Real - Prediccion) ^ 2, na.rm = T),
        SST = sum((Real - mean(Real)) ^ 2, na.rm = T),
        R2 = 1 - SSE / SST,
        RMSE = sqrt(mean((Real - Prediccion) ^ 2, na.rm = T))
      ) %>%
      select(-c(SSE, SST)) %>%
      add_column(!!!results$params) %>%
      add_column(file = results_file)

    measure
  }, warning = function(w) {
    warning(w)
  }, error = function(e) {
    message("Error with ", results_file, ". No predictions")
    warning(e)
  })
}

results_dir <- "./results_paper/linear"
files <- dir(results_dir, "*.rds") %>%
  tibble(file = .) %>%
  mutate(file = file.path(results_dir, file))

results <- files %>%
  rowwise() %>%
  do(metrics = metrics(.$file)) %>%
  ungroup() %>%
  unnest() %>%
  mutate(src = str_remove(src, ".csv")) %>%
  mutate(overlap = str_count(file, "nover") == 0) %>%
  mutate(Algoritmo = if_else(overlap, "LM","LM_nover")) %>%
  select(-overlap)

saveRDS(results, "results_paper/index_linear.rds")