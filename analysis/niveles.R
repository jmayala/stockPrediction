library(ggplot2)
library(dplyr)
library(tidyr)
library(quantmod)
library(stockPrediction)
library(timetk)
library(highcharter)

options(scipen=999)

data <- DJI

niveles <- agrupamiento(data, minimizar = TRUE, coef_promedio = 1, period = xts::to.monthly)
niveles.xts <- lapply(niveles$mean, function(nivel) {
  x <- value_as_xts(nivel, xts_data = data)
  list(period = "monthly", xts = x)
})

chart <- highchart(type = "stock") %>%
  hc_add_series(data, name = "OHLC", color="red", upColor="green") %>%
  hc_title(text = "Niveles") %>%
  hc_tooltip(valueDecimals = 2, split = TRUE) %>%
  hc_legend(enabled = TRUE) %>%
  hc_yAxis(min = min(Cl(data)), max = max(Cl(data)))

Reduce(function(hc, i) {
  nivel <- niveles.xts[[i]]
  hc %>% hc_add_series(name = paste(nivel$period, i), nivel$xts)
}, seq_along(niveles.xts), init = chart)
