library(data.table)
  files <- list.files("results/backtest_opt/buy_and_hold", full.names = TRUE, recursive = TRUE) %>% head()

file <- file.path("results", "backtest_cost0.csv")

initial <- !file.exists(file)

if (initial) {
  load(files[1])
  x <- results$stats
  x$file <- NA
  fwrite(x[0, ], file = file, append = FALSE, col.names = TRUE)
}

i <- 1
n <- length(files)

for (j in seq(i, n)) {
  f <- files[j]
  load(f)
  i <- i + 1
  if (i %% 10 == 0) {
    message("Progress ", i, "/", n)
    message("Loading", f)
  }

  x <- results$stats
  if (nrow(x) == 0) {
    warning("Empty stats ", f)
    next()
  }
  x$file <- basename(f)

  fwrite(x, file = file, append = TRUE, col.names = FALSE)
}
