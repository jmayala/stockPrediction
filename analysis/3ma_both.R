# Ejecuta el 3ma crossover con los parametros por defecto (10, 25, 50) y tambien optimiza

source("analysis/3ma_crossover.R")
source("analysis/3ma_crossover_opt.R")
rm(list=setdiff(ls(), c("default_strategy", "strategies.opt")))

merged <- default_strategy %>% bind_rows(strategies.opt)

merged.sorted <- merged %>%
  arrange(
    desc(Mean.Ann.Sharpe.Ratio),
    desc(Total.Net.Trading.PL),
    desc(Mean.Profit.Factor),
    desc(Mean.Max.Drawdown),
    desc(Total.Gross.Profits),
    Total.Gross.Losses,
    desc(Mean.Gross.Profits),
    Mean.Gross.Losses,
    Mean.Std.Dev.Trade.PL,
    Mean.Std.Err.Trade.PL
  )

merged.sorted[c(1:5, which(merged.sorted$Portfolio == "Portfolio")),]
