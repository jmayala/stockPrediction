library(data.table)
library(lubridate)
library(quantmod)
library(dplyr)
library(stockPrediction)

Sys.setenv(TZ = "UTC")


base_dir <- "~/Downloads/datos_ohlc/intervalo2"
datos <- list.files(base_dir, recursive = TRUE, pattern = "*.csv", full.names = FALSE)


periods <- lapply(datos, function(filename) {
  path <- file.path(base_dir, filename)
  data_xts <- read_ohlcv(path)
  name <- sub(".csv", "", basename(filename))

  p <- tryCatch(xts::periodicity(data_xts), error = function(e) NULL)
  if (is.null(p)) {
    warning(paste(filename, "has no periodicity"))
    return()
  }

  cbind(
    group = stringr::str_extract(name, "^[:alnum:]+_[:alnum:]+"),
    name = name,
    do.call(data.frame, p),
    rows = nrow(data_xts),
    filename = path,
    relative = filename
  )
}) %>% do.call(rbind, .)

outdir <- "~/Downloads/clean_data/intervalo2"

merge_by_group <- function(group) {
  # must be equal for all in group
  same_periodicity <- group %>% select(difftime, frequency, units, scale, label) %>% unique() %>% nrow() == 1
  group_name <- as.character(group$group[1])
  if (!same_periodicity) {
    warning(paste("Group", group_name, "has different periodicities"))
  }

  savedir <- dirname(file.path(outdir, as.character(group$relative[1])))
  if (!dir.exists(savedir)) {
    dir.create(savedir, recursive = TRUE)
  }

  files <- as.character(group$filename)

  all <- Reduce(
    function(all, file) {
      combine_xts(all, read_ohlcv(file))
    },
    files[-1],
    read_ohlcv(files[1])
  )

  path <- file.path(savedir, paste0(group_name, ".csv"))
  export_csv(all, path)
  print(paste("Saved '", group_name, "' in", path))
  group
}

periods %>%
  group_by(group) %>%
  do(merge_by_group(.))
