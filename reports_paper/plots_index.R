# plots
library(stockPrediction)

data_dir <- "~/Downloads/datasets_paper"

load_dataset("ES35_D1", datadir = data_dir)
load_dataset("US30_D1", datadir = data_dir)
load_dataset("GERMAN30_D1", datadir = data_dir)

library(ggplot2)
library(quantmod)
library(tidyquant)
library(highcharter)

df <- ES35_D1 %>%
  to_dataframe() %>%
  mutate(Date = as.Date(Date))

plot_line <- function(xts, title, xbreaks = 5, ybreaks = 5) {
  df <- xts %>%
    to_dataframe() %>%
    mutate(Date = as.Date(Date))

  df %>%
    ggplot(aes(Date, Close)) +
    geom_line() +
    labs(title = title, subtitle = paste(min(df$Date), "/", max(df$Date)), y = "Closing Price", x = "") +
    scale_x_date(breaks = pretty(df$Date, n = xbreaks), expand = c(0, 0)) +
    scale_y_continuous(breaks = pretty(df$Close, n = ybreaks), labels = function(x) format(x, big.mark = ",", scientific = FALSE)) +
    theme_tq()
}
