#' ---
#' title: "MACD Scatter 3d"
#' output: html_document
#' author: ""
#' date: ""
#' ---


#+ setup, include=FALSE
knitr::opts_chunk$set(fig.width = 8, fig.height = 8, echo = FALSE)

library(tidyverse)

stats_file <- "results_paper/backtest_processed.rds"
if (!basename(getwd()) == "stockPrediction") {
  stats_file <- file.path("..", stats_file)
}

stats_all <- read_rds(stats_file) %>%
  unite(params, fast, medium, slow, sep = ",", remove = FALSE) %>%
  mutate(params = str_replace(params, "NA,", ""))

stats <- stats_all %>% filter(cost == 0)
macd <- stats %>% filter(strategy == "macd")


library(scatterplot3d) # load
library(RColorBrewer)
library(viridis)
library(plotly)

map_color <- function(df, var, palette = "Spectral") {
  var <- enquo(var)
  df %>%
    mutate(Colors = brewer.pal(n_distinct(!!var), palette)[match(!!var, unique(!!var))]) %>%
    pull(Colors)
}


map_palette <- function(values, name = "Spectral", ...) {
  val_fact <- as_factor(values)
  n <- length(levels(val_fact))
  if (is.character(name)) {
    colors <- brewer.pal(n, name)
  } else if (is.function(name)) {
    colors <- name(n, ...)
  }

  colors[as.numeric(val_fact)]
}

#'

df_symbol <- macd %>%
  mutate(color = map_palette(Symbol, "Set1")) %>%
  mutate(Symbol = as_factor(Symbol)) %>%
  select(fast, slow, color, Symbol, Profit.Factor, Num.Trades, Profit.To.Max.Draw) %>%
  as.data.frame()

scatterplot3d(df_symbol[, c(1, 2, 5)], pch = 16, color = df_symbol$color, main = "Parameter Search (Profit.Factor)")

legend("bottomright",
       legend = levels(df_symbol$Symbol),
       col = unique(df_symbol$color),
       pch = 16,
       xpd = TRUE,
       title = "Symbol",
       inset = -0.05
)

scatterplot3d(df_symbol[, c(1, 2, 6)], pch = 16, color = df_symbol$color, main = "Parameter Search (Num.Trades)")

legend("bottomright",
       legend = levels(df_symbol$Symbol),
       col = unique(df_symbol$color),
       pch = 16,
       xpd = TRUE,
       title = "Symbol",
       inset = -0.05
)

scatterplot3d(df_symbol[, c(1, 2, 7)], pch = 16, color = df_symbol$color, main = "Parameter Search (Profit.To.Max.Draw)")

legend("bottomright",
       legend = levels(df_symbol$Symbol),
       col = unique(df_symbol$color),
       pch = 16,
       xpd = TRUE,
       title = "Symbol",
       inset = -0.05
)

#' ## Interactive Plots

macd %>%
  mutate(color = map_palette(Symbol, "Set1")) %>%
  plot_ly(x = ~fast, y = ~slow, z= ~Profit.Factor, color = ~Symbol, colors = unique(.$color)) %>%
  add_markers() %>%
  layout(title = "Profit.Factor")

macd %>%
  mutate(color = map_palette(Symbol, "Set1")) %>%
  plot_ly(x = ~fast, y = ~slow, z= ~Num.Trades, color = ~Symbol, colors = unique(.$color)) %>%
  add_markers() %>%
  layout(title = "Num.Trades")

macd %>%
  mutate(color = map_palette(Symbol, "Set1")) %>%
  plot_ly(x = ~fast, y = ~slow, z= ~Profit.To.Max.Draw, color = ~Symbol, colors = unique(.$color)) %>%
  add_markers() %>%
  layout(title = "Profit.To.Max.Draw")

jeje <- macd %>%
  group_by(Symbol) %>%
  nest() %>%
  mutate(
    scatter =
      map2(
        data, Symbol,
        ~ .x %>%
          plot_ly(
            x = ~fast, y = ~slow, z = ~Profit.Factor,
            marker = list(color = ~Profit.Factor, colorscale = c("#FFE1A1", "#683531"), showscale = TRUE)
          ) %>%
          add_markers() %>%
          layout(title = .y)
      )
  )

jeje[1, ]$scatter[[1]]
jeje[2, ]$scatter[[1]]
jeje[3, ]$scatter[[1]]


# macd %>% filter(Symbol == "ES35_D1") -> ES35_macd

# df <- ES35_macd %>%
#   transmute(x = fast, y = slow, z = Profit.Factor) %>%
#   arrange(x, y)
#
# xnew <- seq(min(df$x), max(df$x), len = 100)
# ynew <- seq(min(df$y), max(df$y), len = 100)
# tab <- expand.grid(x = xnew, y = ynew)
#
# model <- NULL
# model <- tryCatch(gam(z ~ te(x, y), data = df), error = function(e) {
#   message("Couldn't create gam model, fallback to linear")
#   tryCatch(lm(z ~ polym(x, y, degree = 1), data = df), error = function(e) {
#     message("Could'nt create linear model")
#   })
# })
#
# if (is.null(model)) {
#   return("")
# }
#
# tab$z <- predict(model, newdata = tab)
# z <- matrix(tab$z, 100, 100)
#
# plot_ly(x = xnew, y = ynew, z = z) %>% add_surface()
