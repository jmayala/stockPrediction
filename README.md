<!-- README.md is generated from README.Rmd. Please edit that file -->
stockPrediction
===============

`stockPrediction` es una herramienta para el analisis y prediccion de precios del Stock Market.

Ejemplos
--------

### Obtener OHLC a partir de un archivo csv

``` r
library(stockPrediction)
csv  <- system.file("extdata", "DJI-2000-2017.csv", package="stockPrediction")
DJI <- read_ohlc(csv)
head(DJI)
#>                Open     High      Low    Close
#> 2000-01-03 11501.85 11522.01 11305.69 11357.51
#> 2000-01-04 11349.75 11350.06 10986.45 10997.93
#> 2000-01-05 10989.37 11215.10 10938.67 11122.65
#> 2000-01-06 11113.37 11313.45 11098.45 11253.26
#> 2000-01-07 11247.06 11528.14 11239.92 11522.56
#> 2000-01-10 11532.48 11638.28 11532.48 11572.20
```

### Grafico de Velas

Usando `quantmod`

``` r
library(quantmod)
chartSeries(DJI[0:20], theme = "white")
```

![](README-candlestick-1.png)

Usando `ggplot`

``` r
library(stockPrediction)
library(ggplot2)
library(timetk)    # xts a tbl
library(tidyquant)

DJI[1:20] %>% tk_tbl(rename_index="Date") %>%
  ggplot(aes(x = Date, y = Close)) +
  geom_candlestick(aes(open = Open, high = High, low = Low, close = Close,
                       color_up = "darkgreen", color_down = "darkred", 
                        fill_up  = "darkgreen", fill_down  = "darkred")) +
  theme_tq() + ggtitle("DJI Enero 2000")
```

![](README-ggplot-1.png)

Build
-----

### Pasos

1.  Abrir el proyecto `.rstudio`
2.  Instalar dependencias del paquete

    ``` r
    install.packages(c("dplyr", "TTR", "RColorBrewer", "ggplot2", "xts", "lubridate", "quantmod", "zoo", "magrittr", "tibble", "tidyquant", "devtools", "roxygen2"))
    ```

3.  Build del paquete

-   `Ctrl` + `SHIFT` + `B`
-   `CMD` + `SHIFT` + `B` (macOS)
-   O desde terminal

    ``` shell
    Rcmd.exe INSTALL --no-multiarch --with-keep.source stockPrediction
    ```

1.  Instalar dependencias de app

    ``` r
    install.packages(c("highcharter", "shiny", "shinydashboard"))
    ```

2.  Ejecutar shiny app

-   `Run app` desde RStudio
-   Desde consola R

    ``` r
    shiny::runApp('app')
    ```
