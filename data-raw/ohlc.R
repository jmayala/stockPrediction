library(stockPrediction)
library(magrittr)

AAPL <- system.file("extdata", "AAPL-2000-2017.csv", package = "stockPrediction") %>% read_ohlc(adjusted = "Adj.Close")
DJI <- system.file("extdata", "DJI-2000-2017.csv", package = "stockPrediction") %>% read_ohlc()
IBEX <- system.file("extdata", "IBEX-2000-2017.csv", package = "stockPrediction") %>% read_ohlc()
DAX <- system.file("extdata", "dax30-d1-1990-2017.csv", package = "stockPrediction") %>% read_ohlc()

# Para cabeceras no estandar
german30d5.csv <- system.file("extdata", "german30d5_test.csv", package = "stockPrediction")
german30d5 <- read_ohlc(german30d5.csv,
  open = "open", high = "high", low = "low", close = "close",
  date = "date", time = "stime"
)

# OHLCV
ES35_D1 <- read_ohlcv(system.file("extdata", "ES35_D1.csv", package = "stockPrediction"))
EURUSD43200 <- read_ohlcv(system.file("extdata", "EURUSD43200.csv", package = "stockPrediction"))

devtools::use_data(AAPL, overwrite = TRUE)
devtools::use_data(DAX, overwrite = TRUE)
devtools::use_data(DJI, overwrite = TRUE)
devtools::use_data(IBEX, overwrite = TRUE)
devtools::use_data(german30d5, overwrite = TRUE)
devtools::use_data(ES35_D1, overwrite = TRUE)
devtools::use_data(EURUSD43200, overwrite = TRUE)
